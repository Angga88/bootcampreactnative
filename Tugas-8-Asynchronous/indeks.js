let readBooks = require('./callback.js')
let books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Tulis code untuk memanggil function readBooks di sini
let time = 10000;
let i=0;
readBooks(time, books[i], function callback(times){
  if (i < books.length-1){
    i+=1;
    time-=times;
    readBooks(times, books[i],callback)
  }
});
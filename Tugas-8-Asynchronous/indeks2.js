let readBooksPromise = require('./promise.js')
 
let books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Tulis code untuk memanggil function readBooksPromise di sini
let time = 10000;
let i=0;
function baca (i, time){
    if(i > books.length-1){
        return ;
    }
    else{
        readBooksPromise(time,books[i])
        .then(function(times){
          return baca(i+1,times)
        })
        .catch(function(error){
            return error

        })
    }
}
// Menjalankan promise
baca(i,time)
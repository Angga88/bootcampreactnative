import React, { Component } from 'react';
import { View, Text, TextInput, Image, StyleSheet, TouchableOpacity} from 'react-native' ;

export default class Login extends Component {
    render() {
        return (
        <View style={styles.container}>
            <View style={{alignItems: "center", padding:35}}>
			<Image
                style={{height:360, width: 380, borderRadius: 8, resizeMode: 'cover'}}
                source={require('./asset/Header.png')}
            />
            </View>
            <Text style={styles.login}>LOGIN</Text>
            <Text style = {styles.textUser}>Username/Email</Text>
            <TextInput	
          		style={{ height: 40,marginHorizontal:20,marginVertical:10, borderColor: "#19C5DD", borderWidth: 1 }}
        	/>
            <Text style = {styles.textUser}>Password</Text>
            <TextInput
          		style={{ height: 40,marginHorizontal:20,marginVertical:10, borderColor: "#19C5DD", borderWidth: 1 }}
        	/>
            <TouchableOpacity
        		style={styles.buttonMasuk}>
        		<Text style = {styles.textTengah} >login</Text>
        	</TouchableOpacity>
            <View style={{alignItems: "center", paddingBottom:35}}>
                <Image
                    style={{height:350, width: 380, borderRadius: 8}}
                    source={require('./asset/Footer.png')}
                />
            </View>
        </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1
    },
    login:{
        color: "#19C5DD",
        textAlign:'left',
        fontSize:23,
        fontWeight:'bold',
        marginHorizontal:20
    },
    textUser:{
        color: 'black',
        fontFamily: 'Roboto',
        textAlign:'left',
        fontSize:15,
        fontWeight:'bold',
        marginHorizontal:20
    },
    buttonMasuk:{
        backgroundColor : "#3EC6FF",
        marginVertical: 10,
        alignItems:'center',
        padding:10,
        borderRadius:12,
        width:150,
        marginHorizontal:120 ,	
    },
    textTengah:{
        color:'white',
        fontFamily: 'Roboto',
        fontSize:20
    }
});
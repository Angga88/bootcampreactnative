import React, { Component } from 'react';
import {View, Image, Text, StyleSheet} from 'react-native';
import { AntDesign } from '@expo/vector-icons';


export default class AboutScreen extends Component {
    render(){
        return (
            <View style={styles.container}>
                <View style={styles.topNavBar}>
                <Image  source={require('./asset/Header2.png')} />
                </View>
                
                <View style={{alignItems: "center", padding:15}}>
                    <Image source={require('./asset/PngItem.png')} style={styles.devImage}/>
                </View>
                <Text style={{fontSize:20, textAlign: "center", fontWeight: "bold"}}>Anggawan</Text>
                <Text style={{fontSize:18, textAlign: "center", fontWeight: "bold", color: '#56B8FF'}}>React Native Developer</Text>
                
                <View style={styles.sosmedCard}>
                    <Text style={styles.cardTitle}>Portofolio</Text>
                    <View style={{height:0.5, backgroundColor:'#56B8FF'}}/>
                    <View style={styles.portofolioContainer}>
                        <View style={styles.account_vert}>
                            <AntDesign style={{margin:5}} name="gitlab" size={40} color="#56B8FF" />
                            <Text>@angga88</Text>
                        </View>
                        
                    </View>
                </View>

                <View style={styles.sosmedCard}>
                    <Text style={styles.cardTitle}>Hubungi Saya</Text>
                    <View style={{height:0.5, backgroundColor:'#56B8FF'}}/>
                    <View style={styles.kontakContainer}>
                        <View style={styles.account_horizontal}>
                            <AntDesign style={{margin:5}} name="facebook-square" size={40} color="#56B8FF" />
                            <Text>Anggawan</Text>
                        </View>
                        <View style={styles.account_horizontal}>
                            <AntDesign style={{margin:5}} name="instagram" size={40} color="#56B8FF" />
                            <Text>@Anggawan</Text>
                        </View>
                        <View style={styles.account_horizontal}>
                            <AntDesign style={{margin:5}} name="linkedin-square" size={40} color="#56B8FF" />
                            <Text>@Anggawan</Text>
                        </View>
                    </View>
                </View>
                <View style={styles.BottomNavBar}>
                <Image source={require('./asset/Footer2.png')} />
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1
    },
    topNavBar: {
        height: 120,
        flexDirection: 'row',
        justifyContent: 'space-around',
        paddingTop: 35,
        elevation: 3
    },
    BottomNavBar: {
        height: 120,
        flexDirection: 'row',
        justifyContent: 'space-around',
        paddingBottom: 35,
        elevation: 3
    },
    devImage:{
        height: 100,
        width: 100,
        borderRadius: 250,
    },
    sosmedCard:{
        backgroundColor: '#EFEFEF',
        borderRadius: 20,
        padding: 15,
        margin: 15
    },
    cardTitle:{
        fontSize: 15,
        fontFamily: 'Roboto',
        color: '#003366'
    },
    portofolioContainer:{
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignContent: "center"
    },
    kontakContainer:{
        justifyContent: 'center',
        paddingLeft: 70
    },
    account_vert:{
        flexDirection: "column",
        alignItems: "center",
    },
    account_horizontal:{
        flexDirection: "row",
        alignItems: "center",
        margin: 5
    }
})

import React from 'react'
import { Button, StyleSheet, Text, View } from 'react-native'



export default function Login({navigation}) {
    console.log(navigation);
    return (
        <View style={styles.container}>
            <Text>Login</Text>
            <Button 
                onPress={()=>navigation.navigate("MyDrawer",{
                    screen: 'App', params: {
                        screen: 'AboutScree'
                    }
                })}
            title="Menuju Halaman HomeScreen" />
        </View>
    )
}

const styles= StyleSheet.create({
    container:{
      flex: 1,
      alignItems: 'center' ,
      justifyContent: 'center'
    }
  })
import React from 'react'
import { StyleSheet, Text, View } from 'react-native';
import { NavigationContainer} from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createStackNavigator } from '@react-navigation/stack';

import HomeScreen from '../Pages/Home';
import AddScreen from '../Pages/AddScreen';
import AboutScreen from '../Pages/AboutScreen';
import LoginScreen from '../Pages/Login';
import ProjectScreen from '../Pages/ProjectScreen';
import Setting from '../Pages/Setting';
import SkillProject from '../Pages/SkillProject';




const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();
const stack = createStackNavigator();

export default function Router() {
    return (
       <NavigationContainer>
           <stack.Navigator>
               <stack.Screen name="LoginScreen" component={LoginScreen}/>
               <stack.Screen name="HomeScreen" component={HomeScreen}/>
               <stack.Screen name="MainApp" component={MainApp}/>
               <stack.Screen name="MyDrawer" component={MyDrawer}/>
           </stack.Navigator>
       </NavigationContainer>
       
    )
}

const MainApp =()=>(
        <Tab.Navigator>
            <Tab.Screen name="AboutScreen" component={AboutScreen} />
            <Tab.Screen name="AddScreen" component={AddScreen} />
            <Tab.Screen name="SkillProject" component={SkillProject} />
        </Tab.Navigator>
)

const MyDrawer =()=> (
    <Drawer.Navigator>
        <Drawer.Screen name="App" component={MainApp} />
        <Drawer.Screen name="AboutScreen" component={AboutScreen} />
    </Drawer.Navigator>
)

const styles = StyleSheet.create({})

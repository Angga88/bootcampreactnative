//Soal No.1 (Range)
function range(startNum, finishNum) {
    var array = [] 
    if(startNum > finishNum){
        for (var i= startNum ; i >= finishNum; i--) {
        array.push(i)
        }
        return array
    } else if (startNum == null || finishNum == null){
        return -1
    } else if (startNum < finishNum) {
        for (var j= startNum ; j <= finishNum; j++) {
            array.push(j)
            }
            return array
    }
}

//Soal 2 (Range with Step)
function rangeWithStep(startNum, finishNum, step) {
    var deret = []
    if(startNum > finishNum){
        for (var i= startNum ; i >= finishNum; i-=step) {
        deret.push(i)
        }
        return deret
    } else if (startNum < finishNum) {
        for (var i= startNum ; i <= finishNum; i+=step) {
        deret.push(i)
        }
        return deret
    }
}

//Soal 3 (Sum of Range)
function sum(startnum, finishnum, step){
	var array = []
	var jumlah = 0 
	if(startnum == null && finishnum == null && step == null){
		return 0
	}else if(step == null){ // range
		if(startnum < finishnum){
			for(var i = startnum ; i <= finishnum ; i++){
				jumlah += i
			}
			return jumlah
		}else if(startnum > finishnum){
			for(var i = startnum ; i >= finishnum ; i--){
				jumlah += i
			}
			return jumlah
		}else if(startnum == null || finishnum == null){
			return 1 
		}  
	}else{		//range with step
		if(startnum < finishnum){ 
			for(var i = startnum ; i <= finishnum ; i+= step){			
				jumlah += i
			}
			return jumlah 
		}else if(startnum > finishnum){
			for(var i = startnum ; i >= finishnum ; i-= step){
				jumlah += i
			}
		return jumlah
		}else if(startnum == null || finishnum == null ){
			return 1 
		}
	}
}
// Soal No.4 (Array Multidimensi)
function dataHandling(data){
    data.forEach(function(nilai) { 
        console.log("Nomor ID: " + nilai [0] + "\n" +
                    "Nama Lengkap: " + nilai [1] + "\n" +
                    "TTL: " + nilai[2] +"," + nilai[3] + "\n" +
                    "Hobi: " + nilai[4] + "\n");
    });

}
// Soal No. 5 (Balik Kata)
function balikKata(string){
    let curStr = string
    let newStr = ""

    for (let i = string.length - 1; i >= 0; i--) {
        newStr = newStr + curStr[i];
    }
    return newStr
}
// Soal No. 6 (Metode Array)
function dataHandling2(data){
    data.splice(1,1, "Roman Alamsyah Elsharawy")
    data.splice(2, 1, "Provinsi Bandar Lampung")
    data.splice(4, 1, "Pria", "SMA Internasional Metro")
    console.log(data);

    //Pemisahan Array Tanggal Lahir
    var tanggal = data[3].split("/")
    
    //Bulan
    var bulan = tanggal[1];
    var namaBulan;
    switch(Number(tanggal[1])) {
        case 1 : { namaBulan = "Januari"; break; }
        case 2 : { namaBulan = "Februari"; break; }
        case 3 : { namaBulan = "Maret"; break; }
        case 4 : { namaBulan = "April"; break; }
        case 5 : { namaBulan = "Mei"; break; }
        case 6 : { namaBulan = "Juni"; break; }
        case 7 : { namaBulan = "Juli"; break; }
        case 8 : { namaBulan = "Agustus"; break; }
        case 9 : { namaBulan = "September"; break; }
        case 10 : { namaBulan = "Oktober"; break; }
        case 11 : { namaBulan = "November"; break; }
        case 12 : { namaBulan = "Desember"; break; }
    }
    console.log(namaBulan);

    //Descending Tanggal
        tanggal.sort(function(a, b){return b-a});
    console.log(tanggal)

    //Tanggal Lahir dd-mm-yyyy
    var forTanggal = data[3].split("/").join("-")
    console.log(forTanggal)

    //Nama
    var nama = data[1].slice(0,14)
    console.log(nama)
}

console.log("-------Soal 1 --------")
console.log("Output Pertama : ")
console.log(range(1, 10))
console.log("Output Kedua : ")
console.log(range(1)) 
console.log("Output Ketiga : ")
console.log(range(11,18)) 
console.log("Output Kemepat : ")
console.log(range(54, 50)) 
console.log("Output Kelima : ")
console.log(range()) 
console.log('\n')
console.log("-------Soal 2 --------")
console.log("Output Pertama : ")
console.log(rangeWithStep(1, 10, 2))
console.log("Output Kedua : ")
console.log(rangeWithStep(11, 23, 3)) 
console.log("Output Ketiga : ")
console.log(rangeWithStep(5, 2, 1))
console.log("Output Keempat : ")
console.log(rangeWithStep(29, 2, 4))
console.log('\n')
console.log("-------Soal 3 --------")
console.log("Output Pertama : ")
console.log(sum(1,10)) 
console.log("Output Kedua : ")
console.log(sum(5, 50, 2)) 
console.log("Output Ketiga : ")
console.log(sum(15,10)) 
console.log("Output Keempat : ")
console.log(sum(20, 10, 2)) 
console.log("Output Kelima : ")
console.log(sum(1)) 
console.log("Output Keenam : ")
console.log(sum()) 
console.log('\n')
console.log("-------Soal 4 --------")
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 
console.log("Output : ")
dataHandling(input)
console.log('\n')
console.log("-------Soal 5 --------")
console.log("Output : ")
console.log(balikKata("Kasur Rusak"))
console.log(balikKata("SanberCode"))
console.log(balikKata("Haji Ijah"))
console.log(balikKata("racecar"))
console.log(balikKata("I am Sanbers"))
console.log('\n')
console.log("-------Soal 6 --------")
var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
console.log("Output : ")
dataHandling2(input)
console.log('\n')
console.log("-------Selesai --------")
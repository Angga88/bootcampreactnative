//Soal No. 1 (Array to Object)
function  arrayToObject(arrData){
    var now = new Date() ;
    var thisYear = now.getFullYear() ;
    var personObj = {} ;
    if(arrData != 0){
       arrData.forEach((d,no) => {
        
        if(d[3]=== undefined || d[3] > thisYear)
        {
          personObj = {firstName: d[0], lastName: d[1], gender: d[2], age: "Invalid Birth Year" };
        }else {
          var ageNow = thisYear - d[3] ;
          personObj = {firstName: d[0], lastName: d[1], gender: d[2], age: ageNow};
        }
         console.log(no+1 + ". " + personObj.firstName + " " + personObj.lastName + ": ", personObj)
      });
    }else{
      console.log('" "');
    }
}

var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]]
console.log("--------Output People---------");
arrayToObject(people)
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
console.log("--------Output People2---------");
arrayToObject(people2)

//Soal No. 2 (Shopping Time)
function shoppingTime(memberId, money) {
    var barang = {
        'Sepatu Stacattu': 1500000,
        'Baju Zoro': 500000,
        'Baju H&N': 250000,
        'Sweater Uniklooh': 175000,
        'Casing Handphone': 50000
    };
    var listPurchased = [];
    if (!memberId) {
        return 'Mohon maaf, toko X hanya berlaku untuk member saja';
    } else if (money < 50000) {
        return 'Mohon maaf, uang tidak cukup';
    } else {
        var modal = money;
        for (const key of Object.keys(barang)) {
            if (modal >= barang[key]) {
                listPurchased.push(key);
                modal -= barang[key];
            }
        }
        return { memberId: memberId, money: money, listPurchased: listPurchased, changeMoney: modal };
    }
}

console.log("-------- Output Soal No. 2 ---------");
console.log(shoppingTime('', 2475000)); 
console.log(shoppingTime('234JdhweRxa53', 15000));
console.log(shoppingTime());

// Soal No. 3 (Naik Angkot)
function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var penumpang = [];

   arrPenumpang.forEach((data) => {
     var jarak= 0 ;
     var RuteAwal  = (element) => element == data [1]
     var RuteAkhir = (element) => element == data [2]
     var asal = rute.findIndex(RuteAwal)
     var tiba = rute.findIndex(RuteAkhir)
     let dataPenumpang = {
       penumpang: data[0],
       naikDari: data[1],
       tujuan: data[2],
       bayar: (tiba - asal) * 2000
      }
        penumpang.push(dataPenumpang)
   });
   return penumpang
}
console.log("------ Output Soal No. 3 --------")
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
console.log(naikAngkot([])); //[]
console.log("------------- Selesai----------------")
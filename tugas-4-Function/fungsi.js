// Soal No.1
function Teriak() {
    var bunyi ="Haloo...Sanbers"
    return bunyi
}
// Soal No.2
function Kalikan(Angka1 , Angka2) {
    var hasil = Angka1 * Angka2
    return hasil
}
// Soal No.3
function introduce(kata1, kata2, kata3, kata4) {
    var kalimat = ("Nama saya "+ kata1 +", umur saya "+ kata2 + " tahun, alamat saya " + kata3 + ", dan saya punya hobby yaitu "+ kata4 + " !! ")
    return kalimat
}
console.log("----------Soal No.1-----------")
console.log(Teriak())
console.log('')
console.log("----------Soal No.2-----------")
var num1 = 12
var num2 = 4
console.log("Hasil perkaliannya adalah " + Kalikan(num1 , num2))
console.log('\n')
console.log("----------Soal No.3-----------")
var Nama = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"
 
var perkenalan = introduce( Nama , age, address, hobby)
console.log(perkenalan)
console.log('\n')
console.log("------------Tugas Selesai---------------")
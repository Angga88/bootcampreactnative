//1. Mengubah fungsi menjadi fungsi arrow
const golden = () => "this is golden!!"
console.log("==========Soal No.1 =============")
console.log(golden());
console.log("=================================")

//2. Sederhanakan menjadi Object literal di ES6
const newfunction = (firstname, lastname) => {
    return {
        fullname() { 
        	return `${firstname} ${lastname}` }
    }
}
console.log("==========Soal No.2 =============")
console.log(newfunction("William", "Imoh").fullname())
console.log("===================================")

//3. Destructuring
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }
  const { firstName, lastName , destination,occupation } = newObject;
  console.log("===========Soal No. 3 =================")
  console.log(firstName, lastName, destination, occupation)
  console.log("===================================")

//4. Array Spreading
console.log("===========Soal No. 4 =================")
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

let combined = [...west, ...east]
console.log(combined);
console.log("===================================")
//5. Template Literals
console.log("===========Soal No. 5 =================")
const planet = "earth"
const view = "glass"
const before = `Lorem ${view} dolor sit amet,consectetur adipiscing elit ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enimad minim veniam`

console.log(before);
console.log("===================================")